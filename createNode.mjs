import { Project, Workspace, NodeType, LinkType } from 'epanet-js'
import fs from "fs";
import express from "express";
import bodyParser from "body-parser";
import net from "net";
import childProcess from "child_process";


const app = express();

app.use(bodyParser.json());
const ws = new Workspace();
const model = new Project(ws);




// Define a route for the API
app.post('/api/sample', (req, res) => {
    const { allData } = req.body;
    const responseData = {
        message: 'API request successful.',
        receivedData: allData
    };
    cretaeNodeInpFile(allData)
    res.json(responseData);
});

// Start the server
const port = 3019;
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});

const junctionStruct = {
    number: '',
    elev: 0,
    demand: 0,
    initQual: '',
    refrance: '',
    connectionIndex: '',
    diraction: '',
};

function cretaeNodeInpFile(dataArray) {
    const net1 = fs.readFileSync("net1.inp");

    ws.writeFile("net1.inp", net1);
    model.open("net1.inp", "report.rpt", "out.bin");
    const nodeData = []

    // // Loop through allData array
    for (const data of dataArray) {
        const { index, refrance, diraction,connectionIndex } = data;
        console.log('connectionIndex Index->',connectionIndex)
        const newValue = { number: index, elev: 700, demand: 100, initQual: '1', refrance: refrance, connectionIndex: connectionIndex !== undefined ? connectionIndex : '', diraction: diraction };
        nodeData.push(newValue)
    }
    console.log('Created array->',nodeData)
    // 
    for (const [index, pipeData] of nodeData.entries()) {

        let isNumaricRef = isNumeric(pipeData.refrance)
        let isNumaricPipe = isNumeric(pipeData.number)

        const n1Index = model.addNode(pipeData.number, NodeType.Junction);
        model.setJunctionData(n1Index, pipeData.elev, pipeData.demand, pipeData.initQual);


        var curentIndexData = 1
        if (index != 0) {
            // set all sub branch from here like (55.1,55.2,55.1.1)
            if (!isNumaricRef) {

                const arrRef = (pipeData.refrance).split(".");
                let isFirstRecordInt = arrRef[arrRef.length - 1];
                arrRef.splice(-1);
                var strRefInt = arrRef.join('.')
                let foundIndex = nodeData.findIndex(function (element) {
                    return element.refrance == strRefInt;
                });
                if (parseInt(isFirstRecordInt) == 1) {
                    // set first node of that branch
                    curentIndexData = foundIndex
                    let currentNodeIndex = model.getNodeIndex(nodeData[index].connectionIndex)
                    let getcordinate = model.getCoordinates(currentNodeIndex)

                    model.addLink((index + 130).toString(), LinkType.Pipe, nodeData[foundIndex].number, nodeData[index].number)

                    if (nodeData[index].diraction == 'W-E') {
                        model.setCoordinates(n1Index, getcordinate.x + (5), getcordinate.y)
                    }else if (nodeData[index].diraction == 'S-N') {
                        model.setCoordinates(n1Index, getcordinate.x, getcordinate.y + ((5)))
                    }else if (nodeData[index].diraction == 'N-S') {
                        console.log('Node ->', getcordinate.x)
                        model.setCoordinates(n1Index, getcordinate.x, (getcordinate.y - 2))
                    }else if (nodeData[index].diraction == 'E-W') {
                        model.setCoordinates(n1Index, getcordinate.x - 5, getcordinate.y)
                    } else {
                        model.setCoordinates(n1Index, getcordinatePrevious.x + 5, getcordinate.y + 5)
                    }


                } else {
                    // set all other node of that branch
                    // console.log("RefInt->",pipeData.refrance)
                    let currentNodeIndex = model.getNodeIndex(nodeData[index].connectionIndex)
                    let getcordinate = model.getCoordinates(currentNodeIndex)

                    let currentNodeIndexPrevious = model.getNodeIndex(nodeData[index - 1].number)
                    let getcordinatePrevious = model.getCoordinates(currentNodeIndexPrevious)

                    model.addLink((index + 130).toString(), LinkType.Pipe, nodeData[index - 1].number, nodeData[index].number)
                    // model.setCoordinates(n1Index, getcordinate.x, index + 5)

                    if (nodeData[index].diraction == 'W-E') {
                        model.setCoordinates(n1Index, getcordinatePrevious.x + (5), getcordinate.y)
                    }else if (nodeData[index].diraction == 'S-N') {
                       model.setCoordinates(n1Index, getcordinate.x, getcordinatePrevious.y + 5)
                    }else if (nodeData[index].diraction == 'N-S') {
                        model.setCoordinates(n1Index, getcordinate.x, (getcordinate.y - 3))
                    }else if (nodeData[index].diraction == 'E-W') {
                        model.setCoordinates(n1Index, getcordinatePrevious.x - 5, getcordinate.y)
                    } else {
                        model.setCoordinates(n1Index, getcordinatePrevious.x + 5, getcordinate.y + 5)
                    }
                }
            } else {
                model.addLink((index + 50).toString(), LinkType.Pipe, nodeData[index - 1].number, nodeData[index].number)
                model.setCoordinates(n1Index, 5.000000 + ((index + 1) * 10 + curentIndexData), 0)
            }
        }
    }
    //add Pipe
    model.addLink('123', LinkType.Pipe, '100000', '1')

    model.saveInpFile('newModel.inp')

    const inp = ws.readFile('newModel.inp', 'utf8')
    // console.log(inp)


    fs.writeFile('/Users/denish/Documents/CurrentProject/ZZ\ Project\ For\ Backup/WSPipeSizer/Node\ examples\ For\ Draw\ Node/model-view-master/public/static/models/sampleNew.inp', inp, err => {
        if (err) {
            console.error(err);
        } else {
            console.error('file written successfully');

        }
        // file written successfully
    });
}



function isNumeric(value) {
    return /^-?\d+$/.test(value);
}














